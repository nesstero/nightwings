/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["../../nightwings/layouts/**/*.{html,js}"],
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        dark: '#1E1E2E',
        gr: '#fab387',
        bl: '#ACB6F4',
      },
    },
  },
}

